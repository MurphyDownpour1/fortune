<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<h1>Добавить приз:</h1>

<?= Yii::$app->session->getFlash('message') ?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($prize, 'prize_name')->textInput()->label('Название') ?>
<?= $form->field($prize, 'prize_quantity')->textInput()->label('Количество') ?>
<?= $form->field($prize, 'prize_value')->textInput()->label('Значение') ?>
<?= $form->field($prize, 'prize_type_id')->dropDownList($prize_types)
                                                ->label('Тип') ?>
<?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
