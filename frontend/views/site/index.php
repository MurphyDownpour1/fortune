<?php

/* @var $this yii\web\View */

$this->title = 'Fortune';
?>

<div id="fortune">
    <div class="main">
        <div id="wallet">
            <div id="money">
                <h2>Ваши деньги:</h2>
                <span>0</span>
            </div>
            <div id="items">
                <h2>Ваши предметы:</h2>
                <span></span>
            </div>
            <div id="bonus">
                <h2>Ваши бонусы:</h2>
                <span>0</span>
            </div>
        </div>
        <div id="loading">
            <img src="uploads/ajax-loader.gif" alt="">
        </div>
        <div id="prize">
            <span>BMW X5</span>
        </div>
        <a href="#" id="spin" class="btn btn-primary">Испытать удачу!</a>
        <div class="actions-container">
            <div id="actions">
                <a href="#" id="accept" class="btn btn-success">Принять</a>
                <a href="#" id="decline" class="btn btn-danger">Отказаться</a>
            </div>
        </div>
    </div>
</div>
