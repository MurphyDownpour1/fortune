<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<h1>Конвертировать деньги в бонусы:</h1>

<h3>Курс: 1 доллар = 10 бонусов</h3>

<div id="wallet">
        <div id="money">
            <h2>Ваши деньги:</h2>
            <span>0</span>
        </div>
        <div id="bonus">
            <h2>Ваши бонусы:</h2>
            <span>0</span>
        </div>
    </div>

<?= Yii::$app->session->getFlash('message') ?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($wallet, 'money')->textInput()->label('Деньги') ?>
<?= $form->field($wallet, 'bonus')->hiddenInput(['value' => $wallet->bonus])->label(false) ?>
<?= Html::submitButton('Конвертировать', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>