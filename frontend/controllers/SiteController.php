<?php

namespace frontend\controllers;

use frontend\models\Prize;
use frontend\models\PrizeType;
use frontend\models\UserPrize;
use frontend\models\Wallet;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\Item;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            Yii::$app->user->loginRequired();
        }
    }

    /**
     * Открывает страницу для создания нового приза
     *
     * @return string
     */
    public function actionAddPrize()
    {
        $prize = new Prize();

        /*
         * получаем типы призов, чтобы дать возможность
            пользователю выбрать тип приза
        */
        $prize_types = [
            '1' => 'Деньги',
            '2' => 'Бонусы',
            '3' => 'Предметы'
        ];

        /* Проверяем, получаем ли данные с формы. Если да, то валидируем и
            сохраняем в базу. А так же выводим сообщение об успешном добавлении */
        if (Yii::$app->request->isPost) {
            $prize->load(Yii::$app->request->post());
            if ($prize->validate()) {
                $prize->save();
                Yii::$app->session
                    ->setFlash('message', 'Приз успешно добавлен!');
            }
        }

        return $this->render('prize', ['prize' => $prize, 'prize_types' => $prize_types]);
    }

    /**
     * Выдаёт случайный приз в JSON-формате, предварительно
     * определив вероятности выпадения призов.
     * Так же заполняем список призов пустыми массивами, чтобы уменьшить шансы
     * на выигрыш.
     *
     * @return \yii\web\Response
     */
    public function actionGiveAPrize()
    {
        $prizes = Prize::find()->orderBy('prize_value')->asArray()->all();

        // Делаем вероятность выигрыша маленькой
        $iterations = count($prizes) * 30;

        // Добавляем пустышки, которые ведут к проигрышу
        for($i = 0; $i <= $iterations; $i++)
        {
            array_push($prizes, ['prize_name' => 'Nothing', 'prize_value' => rand(0.1, 1)]);
        }

        // расставляем вероятности для призов и выбираем случайный
        $probabilities = $this->getProbabilities($prizes, 1/4);
        $prize = $this->getRandomPrize($probabilities);

        if (!empty($prize))
            return $this->asJson($prize);
        else
            return $this->asJson(['error' => 'no prize']);
    }

    /**
     * Вычисляет вероятность выпадения для каждого приза, основываясь на формуле.
     *
     * @param $prizes_arr Массив со всеми призами
     * @param $total_prob Коэффициент периодичности выпадения призов
     * @return array Результирующий массив призов с шансами на выпадение
     */
    private function getProbabilities($prizes_arr, $total_prob)
    {
        $harmonic_sum = 0;
        $result_arr = [];

        /* Складываем обратные значения цен */
        foreach ($prizes_arr as $prize) {
            if ($prize['prize_value'] != 0)
            {
                $harmonic_sum += 1 / $prize['prize_value'];
            }
        }

        /* Пробегаемся по получившимся призам и вычисляем их шанс выпадения */
        foreach ($prizes_arr as $prize)
        {
            if ($prize['prize_value'] != 0)
            {
                $prize['chance'] = $total_prob / ($prize['prize_value'] * $harmonic_sum);
                array_push($result_arr, $prize);
            }
        }

        return $result_arr;
    }

    /**
     * Выдаёт случайный приз из массива
     *
     * @param $prizes Массив призов с шансами на выпадение, которые получены из функции выше
     * @return mixed
     */
    private function getRandomPrize($prizes)
    {
        $randArray = array();

        // тасуем призы
        foreach ($prizes as $prize) {
            for ($i = 0; $i < $prize['chance']; $i++) {
                $randArray[] = $prize;
            }
        }

        // вытаскиваем и возвращаем случайный
        if (!empty($randArray))
            return $randArray[mt_rand(0, count($randArray) - 1)];
        else
            return [];
    }

    /**
     * Если приз был принят, он будет сохранён за пользователем и количество
     * этого приза будет уменьшено в базе на 1
     *
     * @param $id Идентификатор приза
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionAcceptPrize($id)
    {
        $prize_id = $id;
        $user_id = Yii::$app->getUser()->id;

        $userPrize = new UserPrize();
        $userPrize->user_id = intval($user_id);
        $userPrize->prize_id = intval($prize_id);

        if ($userPrize->validate())
        {
            $userPrize->save();
            // обновляем состояние кошелька пользователя
            $userPrize->updatePurse();
            // уменьшаем количество приза на 1
            Prize::decrementQuantity($prize_id);
            return $this->asJson(['response' => 'ok']);
        }
    }

    /**
     * Получаем сведения о кошельке пользователя
     * и возвращаем их
     *
     * @return \yii\web\Response
     */
    public function actionGetWalletInfo()
    {
        $current_user_id = Yii::$app->getUser()->id;
        $wallet = Wallet::findOne(['user_id' => $current_user_id]);

        return $this->asJson($wallet);
    }

    /**
     * Получаем призы-предметы
     *
     * @return \yii\web\Response
     */
    public function actionGetPrizeInfo()
    {
        $current_user_id = Yii::$app->getUser()->id;

        // получаем те призы пользователя, у которых тип = предмет
        $prizes = UserPrize::find()
            ->select('prize.prize_name')
            ->innerJoin('prize', 'prize_to_user.prize_id = prize.id')
            ->innerJoin('user', 'prize_to_user.user_id = user.id')
            ->where(['prize.prize_type_id' => 3, 'user.id' => $current_user_id])
            ->asArray()
            ->all();

        return $this->asJson($prizes);
    }

    /**
     * Конвертация денег в бонусы
     * Сперва получаем кошелёк пользователя, затем создаём его клона и отправляем
     * его в форму. Полученные данные с формы отправляем в модель для конвертации
     *
     * @return string
     */
    public function actionConvertMoneyToBonuses()
    {
        $current_user_id = Yii::$app->getUser()->id;
        $wallet = Wallet::findOne(['user_id' => $current_user_id]);

        $form_wallet = new Wallet();
        $form_wallet->user_id = $wallet->user_id;
        $form_wallet->bonus = $wallet->bonus;
        $form_wallet->money = $wallet->money;

        if (Yii::$app->request->isPost)
        {
            $form_wallet->load(Yii::$app->request->post());
            if ($form_wallet->validate())
            {
                $wallet->convertMoneyToBonuses($form_wallet->money);
            }
        }

        return $this->render('convert', ['wallet' => $form_wallet]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
