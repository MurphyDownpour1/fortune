<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 25.10.2018
 * Time: 0:06
 */

namespace frontend\controllers;

use frontend\models\BankAccount;
use frontend\models\Wallet;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class BankController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return true;
    }

    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Перечисляет все деньги пользователей на их счета
     *
     * @return \yii\web\Response
     */
    public function actionTransferMoney()
    {
        $wallets = Wallet::find()->select('money, user_id')->asArray()->all();

        foreach ($wallets as $wallet)
        {
            $account = BankAccount::findOne(['bank_user_id' => $wallet['user_id']]);
            if ($wallet['money'] > 0)
            {
                $account->bank_money += $wallet['money'];
                $account->validate();
                $account->save();
            }
        }

        Wallet::updateAll(['money' => 0]);

        return $this->asJson(['status' => 'ok']);
    }
}