<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 25.10.2018
 * Time: 0:16
 */

namespace frontend\models;


use yii\db\ActiveRecord;

class BankAccount extends ActiveRecord
{
    public static function tableName()
    {
        return 'api_user_account';
    }

    public function rules()
    {
        return [
            [['bank_number'], 'required'],
            [['bank_money'], 'double'],
            [['bank_number'], 'integer']
        ];
    }
}