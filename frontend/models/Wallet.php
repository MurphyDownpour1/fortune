<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 24.10.2018
 * Time: 15:54
 */

namespace frontend\models;


use yii\db\ActiveRecord;
use Yii;

class Wallet extends ActiveRecord
{
    public static function tableName()
    {
        return 'wallet';
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['money'], 'double'],
            [['bonus', 'user_id'], 'integer'],
        ];
    }

    public function convertMoneyToBonuses($money)
    {
        if (is_numeric($money))
        {
            $money = round($money);
            $this->money -= $money;
            $this->bonus += $money * 10;

            if ($this->save())
            {
                Yii::$app->session->setFlash('message', 'Успешно конвертировано!');
                return $money * 10;
            }
        }
        else
        {
            return false;
        }
    }
}