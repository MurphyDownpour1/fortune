<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 24.10.2018
 * Time: 14:48
 */

namespace frontend\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use Yii;

class UserPrize extends ActiveRecord
{
    public static function tableName()
    {
        return 'prize_to_user';
    }

    public function rules()
    {
        return [
            [['prize_id', 'user_id'], 'required'],
            [['prize_id', 'user_id'], 'integer']
        ];
    }

    public function updatePurse()
    {
        if (!isset($this->prize_id))
            throw new Exception('Prize is not set.');

        $user_id = intval(Yii::$app->getUser()->id);
        $prize = Prize::findOne($this->prize_id);
        $user_wallet = Wallet::findOne(['user_id' => $user_id]);

        switch ($prize->prize_type_id)
        {
            case 1:
                $user_wallet->money += doubleval($prize->prize_value);
                $user_wallet->save();
                break;
            case 2:
                $user_wallet->bonus += intval($prize->prize_value);
                $user_wallet->save();
                break;
        }
    }
}