<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 23.10.2018
 * Time: 11:35
 */

namespace frontend\models;

use yii\db\ActiveRecord;

class Prize extends ActiveRecord
{
    public static function tableName()
    {
        return 'prize';
    }

    /* Правила валидации */
    public function rules()
    {
        return [
            [['prize_name', 'prize_type_id', 'prize_quantity'], 'required'],
            [['prize_name'], 'string'],
            [['prize_type_id', 'prize_quantity'], 'integer'],
            [['prize_value'], 'double'],
        ];
    }

    public static function decrementQuantity($id)
    {
        $prize = self::findOne($id);

        if ($prize->prize_quantity == 1)
        {
            $prize->delete();
        }
        else
        {
            $prize->prize_quantity = --$prize->prize_quantity;
            $prize->update();
        }
    }
}