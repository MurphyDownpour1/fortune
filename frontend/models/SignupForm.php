<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Регистрирует пользователя и создаёт для него кошелёк,
     * а также создаётся банковский счёт
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save())
        {
            $user_id = Yii::$app->db->lastInsertID;

            $wallet = new Wallet();
            $wallet->user_id = $user_id;

            $bank_account = new BankAccount();
            $bank_account->bank_number = rand(1111111111, 99999999999);
            $bank_account->bank_user_id = $user_id;
            $bank_account->bank_money = 0;

            $bank_account->save();

            return $wallet->save() ? $user : null;

        }
    }
}
