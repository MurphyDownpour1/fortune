<?php namespace frontend\tests;

use common\fixtures\WalletFixture;

class WalletTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {

    }

    public function _fixtures()
    {
        return ['wallet' => WalletFixture::className()];
    }

    protected function _after()
    {
    }

    // tests
    public function testConvert()
    {
        $wallet = $this->tester->grabFixture('wallet', 'wallet1');
        expect($wallet->convertMoneyToBonuses(3,1512))->equals(30);
    }
}