updateUserWallet();
updateUserPrize();

$('#spin').click(eventHandler);

$('#accept').click(function (e) {
    var a = $('#accept').attr('value');
    e.preventDefault();
    $.ajax({
        'url': '/accept/' + a,
        'success': function (data) {
            if (data.response == 'ok')
            {
                updateUserWallet();
                updateUserPrize();
                $('#prize span').fadeOut();
                $('#prize span').text('Отлично!').fadeIn();
                $('#spin').bind('click', eventHandler).attr('disabled', false);
                $('.actions-container').fadeOut();
            }
        }
    });
});

$('#decline').click(function (e) {
    e.preventDefault();
    $('#prize span').fadeOut();
    $('#prize span').text('Что ж, попробуем ещё раз?').fadeIn();
    $('#spin').bind('click', eventHandler).attr('disabled', false);
    $('.actions-container').fadeOut();
});

function eventHandler(e) {
    e.preventDefault();
    launch();
}

function launch() {
    $.ajax({
        'url': '/launch',
        'success': function (data) {
            if (data.error == 'no prize' || data.prize_name == 'Nothing')
            {
                $('#prize span').text('Вы ничего не выиграли :(').fadeIn();
                return false;
            }
            if (data.prize_type_id == 1 || data.prize_type_id == 2)
            {
                updateUserWallet();
            }
            $('#spin').unbind('click').attr('disabled', true);
            $('#prize span').fadeOut();
            $('#loading > img, .actions-container').fadeIn();
            updateUserPrize();
            $('#loading > img').fadeOut();
            $('#prize span').text('Вы выиграли ' + data.prize_name + '!').fadeIn();
            $('#accept').attr('value', data.id);
        }
    });
}

function updateUserWallet() {
    $.ajax({
        'url': '/get-wallet',
        'success': function (data) {
            $('#money span').text(data.money);
            $('#bonus span').text(data.bonus);
        }
    });
}

function updateUserPrize() {
    $.ajax({
        'url': '/get-prizes',
        'success': function (data) {
            var items = "";
            data.forEach(function (item) {
                if (item.prize_name != '' || item.prize_name != undefined)
                {
                    items += item.prize_name + '  ';
                }
            });
            $('#items span').text(items);
        }
    });
}