<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_user_account`.
 */
class m181024_201053_create_api_user_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('api_user_account', [
            'id' => $this->primaryKey(),
            'bank_number' => $this->integer()->notNull(),
            'bank_money' => $this->double(),
            'bank_user_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk_bank_to_user',
            'api_user_account',
            'bank_user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('api_user_account');
    }
}
