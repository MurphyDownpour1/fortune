<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wallet`.
 */
class m181024_115331_create_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wallet', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'bonus' => $this->integer(),
            'money' => $this->double()
        ]);

        $this->addForeignKey(
            'fk_user_to_wallet',
            'wallet',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('wallet');
    }
}
