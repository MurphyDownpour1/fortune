<?php

use yii\db\Migration;

/**
 * Handles the creation of table `prize`.
 */
class m181023_072515_create_prize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('prize', [
            'id' => $this->primaryKey(),
            'prize_name' => $this->string()->notNull(),
            'prize_type_id' => $this->integer()->notNull(),
            'prize_value' => $this->double(),
            'prize_quantity' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prize');
    }
}
