<?php

use yii\db\Migration;

/**
 * Handles the creation of table `prize_to_user`.
 */
class m181024_101515_create_prize_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('prize_to_user', [
            'id' => $this->primaryKey(),
            'prize_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk_prize_to_prize',
            'prize_to_user',
            'prize_id',
            'prize',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_user_to_user',
            'prize_to_user',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prize_to_user');
    }
}
