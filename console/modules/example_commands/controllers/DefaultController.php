<?php

namespace app\modules\example_commands\controllers;

use yii\web\Controller;

/**
 * Default controller for the `example_commands` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
