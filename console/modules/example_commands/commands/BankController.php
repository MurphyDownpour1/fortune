<?php

namespace app\modules\example_commands\commands;

use yii\console\Controller;
use Yii;
use frontend\models\Wallet;
use frontend\models\BankAccount;

class BankController extends Controller
{
    public function actionTransfer()
    {

        $wallets = Wallet::find()->select('money, user_id')->asArray()->all();

        foreach ($wallets as $wallet)
        {
            $account = BankAccount::findOne(['bank_user_id' => $wallet['user_id']]);
            if ($wallet['money'] > 0)
            {
                $account->bank_money += $wallet['money'];
                $account->validate();
                $account->save();
            }
        }

        Wallet::updateAll(['money' => 0]);

        echo 'ok';
    }
}