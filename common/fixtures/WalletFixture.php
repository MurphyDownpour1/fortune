<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 24.10.2018
 * Time: 19:43
 */

namespace common\fixtures;

use yii\test\ActiveFixture;

class WalletFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\Wallet';
}