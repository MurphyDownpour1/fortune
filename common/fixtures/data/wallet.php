<?php
return [
    'wallet1' => [
        'user_id' => 1,
        'money' => 200,
        'bonus' => 1000,
    ],
    'wallet2' => [
        'user_id' => 2,
        'money' => 300,
        'bonus' => 4000,
    ],
];